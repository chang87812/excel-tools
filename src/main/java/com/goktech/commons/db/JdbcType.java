/**
 * 
 */
package com.goktech.commons.db;

/**
 * @author Administrator
 *
 */
public enum JdbcType {

	INT,
	LONG,
	DOUBLE,
	STRING,
	DATE
}
