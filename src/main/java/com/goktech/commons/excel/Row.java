package com.goktech.commons.excel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 行元素注解
 * @author 24252
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Row {

	Cell[] cells();
	
	String rowIndex() default "-1";
	
	String height() default "-1";
	
	Style style() default @Style;
	
	String heightInPoints() default "-1";
	
	
}
