/**
 * 
 */
package com.goktech.commons.excel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;

/**
 * excel导出样式
 * 
 * @author echoyu on 2017年10月27日
 *	IndexedColors
 */
@Target({ElementType.FIELD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Style{
	
	/**
	 * 底边框样式
	 * @return
	 */
	short borderBottom() default CellStyle.BORDER_THIN;
	/**
	 * 底边框颜色
	 * @return
	 */
	IndexedColors bottomBorderColor() default IndexedColors.AUTOMATIC;
	
	/**
	 * 左边框
	 * @return
	 */
	short borderLeft() default CellStyle.BORDER_THIN;
	
	/**
	 * 设置左边框颜色
	 * @return
	 */
	IndexedColors leftBorderColor() default IndexedColors.AUTOMATIC;
	
	/**
	 * 设置右边框
	 * @return
	 */
	short borderRight() default CellStyle.BORDER_THIN;
	
	/**
	 * 设置右边框的颜色
	 * @return
	 */
	IndexedColors rightBorderColor() default IndexedColors.AUTOMATIC;
	
	/**
	 * 设置顶边框
	 * @return
	 */
	IndexedColors topBorderColor() default IndexedColors.AUTOMATIC;
	
	/**
	 * 设置顶边框
	 * @return
	 */
	short borderTop() default CellStyle.BORDER_THIN;
	
	/**
	 * 设置自动换行
	 * @return
	 */
	boolean wrapText() default false;
	
	/**
	 * 水平对齐的样式为居中对齐
	 * @return
	 */
	short alignment() default CellStyle.ALIGN_CENTER;
	
	/**
	 * 设置垂直对齐的样式为居中对齐
	 */
	short verticalAlignment() default CellStyle.ALIGN_GENERAL;
	
	/**
	 * 全局默认时间格式
	 * @return
	 */
	String dateFormat() default "yyyy-MM-dd HH:mm:ss";
	
	IndexedColors fillForegroundColor() default IndexedColors.WHITE;
	
	IndexedColors fillBackgroundColor() default IndexedColors.WHITE;
	
	short fillPattern() default CellStyle.SOLID_FOREGROUND;
	
	Font font() default @Font;
	
	String id() default "";
}
