/**
 * 
 */
package com.goktech.commons.excel;

import org.apache.poi.ss.usermodel.CellStyle;

/**
 * excel导出样式解析器 接口
 * 
 * @author echoyu on 2017年10月27日
 *
 */
public interface ExcelStyle {
	
	/**
	 * 获取默认的全局样式
	 * @return
	 */
	public CellStyle getDefaultStyle();
	
	
	/**
	 * 获取表头样式注解
	 * @return
	 */
	public CellStyle getTitleStyle(String name);
	
	/**
	 * 获取样式
	 * @param style
	 * @return
	 */
	public CellStyle getStyle(Style style);
	
	public CellStyle getStyle(CellStyle cellStyle);
	
	public CellStyle getStyle(String id);
}
