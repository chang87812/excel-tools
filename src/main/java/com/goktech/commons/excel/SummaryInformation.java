package com.goktech.commons.excel;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 生成文件信息的注解
 * @author 24252
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SummaryInformation {
	
	//设置xls文件的作者信息
	String author() default "zhongmh";
	
	//创建程序信息
	String applicationName() default "excel util";
	
	//文件最后保存折信息
	String lastAuthor() default "zhongminghong";
	
	//增加文件作者信息
	String comments() default "我是笨蛋";
	
	//增加文件的标题信息
	String title() default "标题";
	
	//增加文件标题信息
	String subject() default "增加文件主题信息";
	
	boolean createDateTime() default false;
	/**
	 * 自适应大小
	 * @return
	 */
	boolean autoSizeColumn() default true;
}
