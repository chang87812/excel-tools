/**
 * 
 */
package com.goktech.commons.excel;

/**
 * @author zhongmh
 * @email yunmaozj@163.com
 * @createTime 2018年1月2日下午5:42:20
 * @desc 
 */
public class Title {

	/**
	 * 对应的字段名 id
	 */
	private String fieldName;
	
	/**
	 * 对应的值 excle在表中的名字
	 */
	private String fieldvalue;

	
	public Title(String fieldName, String fieldvalue) {
		super();
		this.fieldName = fieldName;
		this.fieldvalue = fieldvalue;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldvalue() {
		return fieldvalue;
	}

	public void setFieldvalue(String fieldvalue) {
		this.fieldvalue = fieldvalue;
	}
	
	
}
