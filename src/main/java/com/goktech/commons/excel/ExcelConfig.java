/**
 * 
 */
package com.goktech.commons.excel;

/**
 * @author 24252
 * excel导出参数配置
 */
public class ExcelConfig {

	//是否显示每个字段的名字
	private boolean showTitle = true;

	public boolean isShowTitle() {
		return showTitle;
	}

	public void setShowTitle(boolean showTitle) {
		this.showTitle = showTitle;
	}
	
	
}
