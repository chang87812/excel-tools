package com.goktech.commons.excel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * excel导出样式
 * 
 * @author echoyu on 2017年10月27日
 *
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Excel {
	
	String name() default "";
	
	String defaultValue() default "";
	
	int sort() default 10;
	
	Style style() default @Style;
}
