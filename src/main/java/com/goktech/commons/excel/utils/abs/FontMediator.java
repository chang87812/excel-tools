/**
 * 
 */
package com.goktech.commons.excel.utils.abs;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;

import com.goktech.commons.excel.utils.FontMediatorInterface;

/**
 * @author zhongmh
 *
 */
public class FontMediator<M> implements FontMediatorInterface<FontMediator<M>, M>{

	private Font font;
	
	private CellStyle cellStyle;
	
	private Workbook workbook;
	
	private M m;
	
	
	public FontMediator(CellStyle cellStyle, Workbook workbook, M m) {
		super();
		this.cellStyle = cellStyle;
		this.workbook = workbook;
		this.font = this.workbook.createFont();
		this.m = m;
	}
	
	public FontMediator(Font font, CellStyle cellStyle, M m) {
		super();
		this.font = font;
		this.cellStyle = cellStyle;
		this.m = m;
	}


	@Override
	public M conver() {
		this.cellStyle.setFont(font);
		return m;
	}

	@Override
	public FontMediator<M> fontName(String fontName) {
		this.font.setFontName(fontName);
		return this;
	}

	@Override
	public FontMediator<M> italic(boolean italic) {
		this.font.setItalic(italic);
		return this;
	}

	@Override
	public FontMediator<M> height(short fontHeight) {
		this.font.setFontHeight(fontHeight);
		return this;
	}

	@Override
	public FontMediator<M> heightInPoints(short heightInPoints) {
		this.font.setFontHeightInPoints(heightInPoints);
		return this;
	}

}
