/**
 * 
 */
package com.goktech.commons.excel.utils.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.goktech.commons.excel.utils.ExcelReader;
import com.goktech.commons.excel.utils.IRowReader;
import com.goktech.commons.excel.utils.RowReaderFactory;
import com.goktech.commons.excel.utils.abs.AbstractReader;

/**
 * @author 24252 海量数据导入
 */
public class ExcelBigDataImport extends AbstractReader<ExcelBigDataImport> {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private List<String> titleList = new ArrayList<>();
	/**
	 * excel 读取器
	 */
	private ExcelReader excelReader;
	/**
	 * 行分发器
	 * 
	 * @param clazz
	 * @param is
	 */
	private IRowReader readerFactory = null;

	public ExcelBigDataImport(Class<?> clazz, InputStream is) {
		super(clazz,is);
		excelReader = new ExcelReader();
		try {
			readerFactory = new RowReaderFactory<ExcelBigDataImport>(this);
			excelReader.setIRowReader(readerFactory);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ExcelBigDataImport(Class<?> clazz,File file) throws FileNotFoundException {
		this(clazz,new FileInputStream(file));
	}

	public ExcelBigDataImport(Class<?> clazz,String path) throws FileNotFoundException {
		this(clazz,new File(path));
	}

	public List<?> getList() {
			return super.getList();	
	}

	@Override
	public ExcelBigDataImport getSelf() {
		return this;
	}
	
	@Override
	public ExcelBigDataImport initTitle(List<String> rowList) {
		this.titleList.addAll(rowList);
		return super.initTitle(rowList);
	}

	@Override
	public ExcelBigDataImport run() {
		try {
			excelReader.process(is);
		} catch (Exception e) {
			e.printStackTrace();
			this.getSelf();
		}
		return this.getSelf();
	}
}
