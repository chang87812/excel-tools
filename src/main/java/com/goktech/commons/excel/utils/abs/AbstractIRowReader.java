/**
 * 
 */
package com.goktech.commons.excel.utils.abs;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.goktech.commons.excel.utils.IRowReader;

/**
 * @author 24252
 * 提供线程池和计数器
 */
public abstract class AbstractIRowReader implements IRowReader{
	
	public ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

	public AtomicInteger atomicInteger = new AtomicInteger(0);

	public AtomicInteger countInteger = new AtomicInteger(0);

	public AtomicLong time = new AtomicLong(0);

	public AbstractIRowReader() {
		super();
	}
}
