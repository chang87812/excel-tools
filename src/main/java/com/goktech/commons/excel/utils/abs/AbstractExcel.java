/**
 * 
 */
package com.goktech.commons.excel.utils.abs;

import com.goktech.commons.utils.PropertyUtils;

/**
 * @author 24252
 * 高级excel抽象对象
 */
public abstract class AbstractExcel<T> {

	public Class<?> clazz = null;
	
	protected PropertyUtils propertyUtils = null;
	
	public AbstractExcel(Class<?> clazz){
		this.clazz = clazz;
		this.propertyUtils = new PropertyUtils(clazz);
	}
	
	public T setClass(Class<?> clazz){
		this.clazz = clazz;
		this.propertyUtils = new PropertyUtils(clazz);
		return getSelf();
	}
	public abstract T getSelf();
}
