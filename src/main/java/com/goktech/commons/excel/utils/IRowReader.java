/**
 * 
 */
package com.goktech.commons.excel.utils;

import java.util.List;

/**
 * @author 24252
 * 处理解析器返回的数据
 */
public interface IRowReader {

	public void getRows(int sheetIndex,int curRow, List<String> rowlist);
	
	public boolean isEnd();
	
	public void shutDown();
}
