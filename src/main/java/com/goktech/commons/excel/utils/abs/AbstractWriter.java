/**
 * 
 */
package com.goktech.commons.excel.utils.abs;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.goktech.commons.excel.utils.ValueSelector;

/**
 * @author Administrator 导出可以 使用json数据 对于类也要转换成这样的List<List<Object>>
 */
public abstract class AbstractWriter<T> implements ExcelWriter<T> {

	protected List<String> titles = new ArrayList<>();

	protected Collection<Collection<Object>> content = new ArrayList<>();

	protected Workbook book;

	protected Sheet sheet;

	protected int index;
	
	public AbstractWriter(){
		this.book = new XSSFWorkbook();
	}
	
	public T put(List<String> titles, List<List<Object>> content) {
		return this.getSelf();

	}

	@Override
	public T setTitle(Collection<String> collection) {
		Iterator<String> iterator = collection.iterator();
		while(iterator.hasNext()) {
			this.titles.add(iterator.next());
		}
		return this.getSelf();
	}

	@Override
	public T setContent(Collection<Collection<Object>> collection) {
		this.content = collection;
		return this.getSelf();
	}

	@Override
	public T createSheet(String sheetName) {
		this.sheet = this.book.createSheet(sheetName);
		return this.getSelf();
	}
	
	protected void createRow(CellStyle style,Collection<Object> collections) {
		Iterator<Object> iterator  = collections.iterator();
		Row row = createRow();
		int index = 0;
		while(iterator.hasNext()) {
			createCell( row, index++, style, iterator.next());
		}
	}
	protected void createRow(Collection<Object> collections) {
		this.createRow(null, collections);
	}
	protected Row createRow() {
		Row row = this.sheet.createRow(index++);
		return row;
	}
	
	protected Cell createCell(Row row,int index,CellStyle style,Object value) {
		Cell cell = row.createCell(index);
		if(style != null)
			cell.setCellStyle(style);;
		ValueSelector.setValue(value, cell);
		return cell;
	}
	
	//常见表头
	protected T createTile() {
		Row row = this.createRow();
		for(int index = 0;index<titles.size();index++) {
			createCell(row, index, null, titles.get(index));
		}
		return this.getSelf();
		
	}
	
	protected T createContent() {
		for(Collection<Object> list : content) {
			createRow(list);
		}
		return this.getSelf();
	}

	@Override
	public void write(OutputStream os) {
		try {
			loadData();
			this.book.write(os);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 装载数据的数据方法
	 */
	private void loadData() {
		if (this.sheet == null)
			this.createSheet("表格");
		if(titles.size() > 0) {
			createTile();
		}
		if(content.size() > 0) {
			createContent();
		}
	}

	public void write(String path) {
		write(new File(path));
	}

	public void write(File file) {
		try {
			write(new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public abstract T getSelf();

}
