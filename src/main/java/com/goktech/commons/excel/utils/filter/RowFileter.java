/**
 * 
 */
package com.goktech.commons.excel.utils.filter;

import java.util.ArrayList;
import java.util.List;

import com.goktech.commons.utils.StringUtils;

/**
 * @author 24252
 *
 */
public class RowFileter {
	
	/**
	 * 判断多余的列,以第一列为准
	 * @param length 第一列的长度
	 * @param rowList
	 * @return
	 */
	public List<String> Center(int length,List<String> rowList){
		if(rowList != null && rowList.size() == length){
			return rowList;
		}else if(rowList != null && rowList.size() > length){
			if(length == 0){
				throw new IllegalArgumentException("excel表的初始长度为不能0");
			}
			return rowList.subList(0, length);
		}
		return new ArrayList<String>(1);
	}
	
	/**
	 * 空行判断 	全部为空返回false ,
	 * @param rowList
	 * @return
	 */
	public boolean nullFileter(List<String> rowList){
		for(int i=0;i<rowList.size();i++){
			if(!StringUtils.isEmpty(rowList)){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 去掉尾部的空格
	 * @return
	 */
	public List<String> subEndEmpty(List<String> rowList){
		List<String> tmpList = new ArrayList<String>();
		for(String str : rowList){
			if(!StringUtils.isEmpty(str)){
				tmpList.add(str);
			}
		}
		return tmpList;
		
	}
}
