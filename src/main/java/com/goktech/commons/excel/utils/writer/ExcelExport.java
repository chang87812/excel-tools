/**
 * 
 */
package com.goktech.commons.excel.utils.writer;

import java.util.ArrayList;
import java.util.List;

import com.goktech.commons.excel.utils.abs.AbstractWriter;

/**
 * @author Administrator
 *
 */
public class ExcelExport extends AbstractWriter<ExcelExport> {

	@Override
	public ExcelExport getSelf() {
		return this;
	}

	public ExcelExport addTitle(String[] titles) {
		List<String> list = new ArrayList<>();
		for (String titel : titles)
			list.add(titel);
		super.setTitle(list);
		return this;
	}

	public ExcelExport initTitle(String[] titles) {
		this.titles.clear();
		this.addTitle(titles);
		return this;
	}

	// 增加一行内容
	public ExcelExport addContent(String[] contents) {
		List<Object> list = new ArrayList<>();
		for (String content : contents)
			list.add(content);
		this.content.add(list);
		return this;
	}

	// 增加一行内容
	public ExcelExport initContent(String[] contents) {
		this.content.clear();
		this.addContent(contents);
		return this;
	}

}
