/**
 * 
 */
package com.goktech.commons.ppt;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;

/**
 * @author zhongmh
 * @email yunmaozj@163.com
 * @createTime 2017年12月27日下午5:36:44
 * @desc
 */
public class PPTXUtils {

	public void create() {
		try {
			XMLSlideShow xmlSlideShow = new XMLSlideShow(new FileInputStream("E:\\360Downloads\\10 述职报告模板【41套】——亮亮图文旗舰店\\述职报告 (1).pptx"));

			XSLFSlide[] slides = xmlSlideShow.getSlides();
			XSLFSlide slide = slides[2];

			BufferedImage img = new BufferedImage(900, 500, BufferedImage.TYPE_INT_RGB);
			Graphics2D graphics = img.createGraphics();
			graphics.setPaint(Color.WHITE);
			graphics.fill(new Rectangle2D.Float(0, 0, 900, 500));
			slide.draw(graphics);

			FileOutputStream out = new FileOutputStream("D:/1.jpeg");
			javax.imageio.ImageIO.write(img, "jpeg", out);
			out.close();

			System.out.println("缩略图成功!");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
