/**
 * 
 */
package com.yunmaozj.utils;

import java.io.FileNotFoundException;
import java.util.List;

import org.junit.Test;

import com.goktech.commons.excel.utils.reader.ExcelImport;
import com.yunmaozj.bean.Government;

/**
 * @author Administrator
 *
 */
public class TestExcelImport {

	private final String path = "政府官网信息表.xlsx";
	
	@Test
	public void testImport() throws FileNotFoundException {
		List<Government> list = (List<Government>) new ExcelImport(Government.class, path).run().getList();
		System.out.println(list.size());
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		new TestExcelImport().testImport();
	}
}
