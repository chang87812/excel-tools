package com.yunmaozj.bean;

import com.goktech.commons.excel.Excel;

public class Area {

    @Excel(name = "省级地区")
    private String province;
    @Excel(name = "市级地区")
    private String city;
    @Excel(name = "区县级地区")
    private String county;
    @Excel(name = "各市数量")
    private int number;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Area{" +
                "province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", number=" + number +
                '}';
    }
}
