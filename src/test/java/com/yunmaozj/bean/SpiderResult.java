/**
 * 
 */
package com.yunmaozj.bean;

import com.goktech.commons.excel.Excel;

/**
 * @author Administrator
 *
 */
public class SpiderResult{

	@Excel(name="区域等级")
	private String areaGrade;
	@Excel(name="种类")
	private String type;
	@Excel(name="原URL")
	private String sourceURl;
	@Excel(name="关键字")
	private String keyWord;
	@Excel(name="地址")
	private String url;
	@Excel(name="标签")
	private String text;
	public String getAreaGrade() {
		return areaGrade;
	}
	public void setAreaGrade(String areaGrade) {
		this.areaGrade = areaGrade;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSourceURl() {
		return sourceURl;
	}
	public void setSourceURl(String sourceURl) {
		this.sourceURl = sourceURl;
	}
	public String getKeyWord() {
		return keyWord;
	}
	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public SpiderResult(String areaGrade, String type, String sourceURl, String keyWord, String url, String text) {
		super();
		this.areaGrade = areaGrade;
		this.type = type;
		this.sourceURl = sourceURl;
		this.keyWord = keyWord;
		this.url = url;
		this.text = text;
	}
	public SpiderResult() {
		super();
	}

	
}
