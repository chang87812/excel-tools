## charge分支报表升级功能说明	
### 导出测试代码
	//导出表头测试
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String date = df.format(new Date());
		//生成收费项信息
		List<Title> titles = new ArrayList<Title>();
		List<BbCurrentPeriodImpl> data = new ArrayList<>();
		for(int i=0;i<10;i++){
			Title title = new Title();
			title.setFieldvalue("表头->"+i);
			title.setFieldName("value->"+i);
			titles.add(title);
			BbCurrentPeriodImpl bbCurrentPeriodImpl = new BbCurrentPeriodImpl();
			bbCurrentPeriodImpl.setId(UUID.randomUUID().toString().replaceAll("-", ""));
			bbCurrentPeriodImpl.setName("钟明宏->"+i);
			Map<String,Object> map = new HashMap<String,Object>(); 
			for(int j=0;j<10;j++){
				map.put("value->"+j, new BigDecimal(Math.random()*100));
			}
			bbCurrentPeriodImpl.setItems(map);
			data.add(bbCurrentPeriodImpl);
		}
		Map<String,Object> dataMap = new HashMap<String,Object>();
		dataMap.put("title.length", titles.size()+2);
		dataMap.put("r_e_1", 19);//设置数据
		dataMap.put("r_e_2", 20);//设置数据
		dataMap.put("end", 19);//设置数据
		ExcelXSSFExport excelXSSFExport = new ExcelXSSFExport(BbCurrentPeriodImpl.class,dataMap);//设置需要解析的实体类
		excelXSSFExport.putData(dataMap);//放置数据，也可以放在构造参数中
		excelXSSFExport.createSheet("第一个工作薄")//创建工作薄必须做，可以创建多个工作薄
		.initStyle()//初始化样式功能,必须在初始化定义行和数据之前调用
		.initRows()//初始化定义行
		.addTitle(1,titles)//添加定义表头，第一个参数为添加的位置,
		.initTitle()//初始化表头
		.forEach(data)//初始化数据
		;
		excelXSSFExport.write(new FileOutputStream(new File("E:\\报表导出表头测试"+date+".xlsx")));
### 自定义表头说明
	自定义表头需要注解@Titles
	目前只能放在map类型的字段上面,也就是说实体类里面必须有个Map类型的字段，
	后面填数据也会从map里面获取
	对于添加的表头的值，需要放入Title(com.goktech.commons.excel.Title)实体类中,可以参考上面的测试代码
### 自定义行说明
	@Font(fontName = "华文楷体")
	@Style//定义全局样式
	@SummaryInformation(author = "钟明宏", title = "房屋缓存表", applicationName = "Rms系统")
	@Rows(rows = { 
					@Row(cells = {
						@Cell(firstCol = "1", lastCol = "${title.length}",value = "XX项目预估收入调整入账报表")}
						,firstRow = "1", lastRow = "2",style=@Style),
					@Row(cells = { 
						@Cell(firstCol = "1", lastCol = "2", value = "制表时间:"),
						@Cell(firstCol = "3", lastCol = "4", value = "${createTime}") 
						}, firstRow = "3", lastRow = "4"), 
					@Row(cells = {
						@Cell(firstCol = "1", lastCol = "2", value = "凭证日期："),
						@Cell(firstCol = "3", lastCol = "5", value = "${time}",style=@Style(fillBackgroundColor=IndexedColors.ROSE,fillForegroundColor=IndexedColors.BLUE_GREY))
						}, firstRow = "5", lastRow = "7"),
					@Row(cells = { //定义最后几行
							@Cell(firstCol = "2", lastCol = "3", value = "制单:"),
							@Cell(firstCol = "7", lastCol = "8", value = "审核:") 
							}, firstRow = "${r_e_1}", lastRow = "${r_e_2}",
							rowIndex="${end}"),//定义未知行尾	
					})
以上代码添加到方法类上面
---
### 其他注解代码
	@Style(fillForegroundColor=IndexedColors.RED,fillBackgroundColor=IndexedColors.RED)
		@Excel(name="id")
		private String id;
		@Excel(name="名称")
		private String name;
		
		@Style(fillForegroundColor=IndexedColors.RED,fillBackgroundColor=IndexedColors.RED)
		@Titles
		private Map<String,Object> items;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public Map<String, Object> getItems() {
			return items;
		}
		public void setItems(Map<String, Object> items) {
			this.items = items;
		}
##### 以上代码测试输出结果
![Alt text](http://cqwzxt.oss-cn-shenzhen.aliyuncs.com/resources/2017/TIM%E6%88%AA%E5%9B%BE20180104110130.png "结果")

### 其他功能说明
+ 对于复杂的行,开放了，创建行的方法，可以自行创建行，填数据，行的索引默认递增

### 待解决问题
+ 样式优化
+ 异常处理
